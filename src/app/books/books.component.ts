import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { AngularFireDatabase } from '@angular/fire/database';
import { BooksService } from '../books.service';

@Component({
  selector: 'books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {

  nickname;
  user = [];
  books = [];

  bookName = '';
  author = '';
  read:boolean;


  addBook(){
    this.bookService.addBook(this.bookName, this.author);
    this.bookName = '';
    this.author = '';    
  }

  constructor(public authService: AuthService, private db:AngularFireDatabase, private bookService: BooksService) { }

  ngOnInit() {
    this.authService.user.subscribe(user => {
      this.db.list('/users/'+user.uid).snapshotChanges().subscribe(
        users => {
          this.user = [];
          console.log("user:" + this.user);
              users.forEach(
                user => {
                  let y = user.payload.toJSON();
                  console.log("y:" + y);
                  y["$key"] = user.key;
                  this.user.push(y);
                 
                  console.log("user:" + this.user);
            } 
          )
          
         
        }
      )
      this.db.list('/users/'+user.uid+'/books').snapshotChanges().subscribe(
        books => {
          this.books = [];
          books.forEach(
            book => {
              let b = book.payload.toJSON();
              b['key'] = book.key;
              this.books.push(b);
              console.log(this.books); 
            }

          )
        }
      )
      
        
      }
     
    )
    
  }
}
