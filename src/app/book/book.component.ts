import { Component, OnInit, Input } from '@angular/core';
import { BooksService } from '../books.service';

@Component({
  selector: 'book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.css']
})
export class BookComponent implements OnInit {
 read2:boolean;
  tempText1;
  tempText2;
  showTheButton = false;

  @Input() data:any;

  bookName = '';
  author = '';
  key = '';
  read:boolean;
  showEditField=false;
  showAuthor = false;
  deleteBut = false; 

  showEdit(){
    this.showEditField = true;
    this.tempText1 = this.bookName;
    this.tempText2= this.author;
    this.showAuthor = false;
   
  }
  
save(){
  this.booksService.update(this.key,this.tempText1,this.tempText2);
  this.showEditField = false;
  }
  cancel()
  {
   this.showAuthor = false;
   this.showEditField = false;
   this.bookName = this.tempText1;
   this.author = this.tempText2;
  }

  checkChange()
  {
    this.booksService.updateRead(this.key,this.read);
  }

  showButton()
  {
    this.showAuthor = true;
  }

  hideButton()
  {
    this.showAuthor = false;
  }

  show2Buttons()
  {
    this.deleteBut = true;
  }

  deleteItem()
  {
    this.booksService.deleteBook(this.key);
  }


  constructor(private booksService:BooksService) { }

  ngOnInit() {

    this.bookName = this.data.bookName;
    this.author = this.data.author;
    this.read = this.data.read;
    this.key = this.data.key;
  }

}
